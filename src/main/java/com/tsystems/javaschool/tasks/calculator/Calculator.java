package com.tsystems.javaschool.tasks.calculator;
import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        return calculate(statement);
    }
    public static String calculate(String statement)
    {
        try
        {
            while (statement.contains("("))
            {
                String ss = statement.substring(statement.lastIndexOf("("));
                ss = ss.substring(0, ss.indexOf(")") + 1);
                String sss = ss.substring(1, ss.length() - 1);
                double ssss = calculateWithoutParentheses(sss);
                //ifNegative
                if (ssss >= 0 || statement.lastIndexOf("(") == 0)
                {
                    statement = statement.replace(ss, Double.toString(ssss));
                }
                else
                {
                    if (statement.charAt(statement.lastIndexOf("(") - 1) == '+')
                    {
                        ss = statement.charAt(statement.lastIndexOf("(") - 1) + ss;
                        statement = statement.replace(ss, Double.toString(ssss));
                    }
                    else if (statement.charAt(statement.lastIndexOf("(") - 1) == '-') {
                        ss = statement.charAt(statement.lastIndexOf("(") - 1) + ss;
                        statement = statement.replace(ss, "+" + Double.toString(ssss * (-1)));
                    }
                    else if (statement.charAt(statement.lastIndexOf("(") - 1) == '(' || statement.charAt(statement.lastIndexOf("(") - 1) == '*' || statement.charAt(statement.lastIndexOf("(") - 1) == '/')
                    {
                        statement = statement.replace(ss, Double.toString(ssss));
                    }
                }
            }
            return rounding(Double.toString(calculateWithoutParentheses(statement)));
        }
        catch (Exception e)
        {
            return  null;
        }
    }
    public static double calculateWithoutParentheses(String s)
    {
        String ss = s.substring(1);
        if (!ss.contains("+") && !ss.contains("-") && !ss.contains("*") && !ss.contains("/"))
        {
            return Double.parseDouble(s);
        }
        else {
            //recognizing Doubles
            ArrayList<Integer> Indexes = new ArrayList<Integer>();
            for (int i = 1; i < s.length(); i++) {
                if (s.charAt(i) == '*' || s.charAt(i) == '/' || s.charAt(i) == '+') {
                    Indexes.add(i);
                }
                else if (s.charAt(i) == '-' && s.charAt(i-1) != '*' && s.charAt(i-1) != '/')
                {
                    Indexes.add(i);
                }
            }

            ArrayList<Double> doubles = new ArrayList<Double>();
            doubles.add(Double.parseDouble(s.substring(0, Indexes.get(0))));
            for (int i = 1; i < Indexes.size(); i++)
            {
                if (s.charAt(Indexes.get(i - 1)) == '-')
                    doubles.add(Double.parseDouble(s.substring(Indexes.get(i - 1), Indexes.get(i))));
                else
                    doubles.add(Double.parseDouble(s.substring(Indexes.get(i - 1) + 1, Indexes.get(i))));
            }
            if (s.charAt(Indexes.get(Indexes.size() - 1)) == '-')
                doubles.add(Double.parseDouble(s.substring(Indexes.get(Indexes.size() - 1))));
            else
            {
                doubles.add(Double.parseDouble(s.substring(Indexes.get(Indexes.size() - 1) + 1)));
            }

            ArrayList<Integer> removeList = new ArrayList<Integer>();

            //division
            for (int i = 0; i < Indexes.size(); i++) {
                if (s.charAt(Indexes.get(i)) == '/') {
                    doubles.set(i + 1, doubles.get(i) / doubles.get(i + 1));
                    removeList.add(i);
                }
            }
            for (int i = 0; i < removeList.size(); i++) {
                doubles.remove(removeList.get(i) - i);
                Indexes.remove(removeList.get(i) - i);
            }
            removeList.clear();

            //multiplying
            for (int i = 0; i < Indexes.size(); i++) {
                if (s.charAt(Indexes.get(i)) == '*') {
                    doubles.set(i + 1, doubles.get(i) * doubles.get(i + 1));
                    removeList.add(i);
                }
            }
            for (int i = 0; i < removeList.size(); i++) {
                doubles.remove(removeList.get(i) - i);
                Indexes.remove(removeList.get(i) - i);
            }
            removeList.clear();

            //composition
            for (int i = 0; i < Indexes.size(); i++) {
                if (s.charAt(Indexes.get(i)) == '+') {
                    doubles.set(i + 1, doubles.get(i) + doubles.get(i + 1));
                    removeList.add(i);
                }
            }
            for (int i = 0; i < removeList.size(); i++) {
                doubles.remove(removeList.get(i) - i);
                Indexes.remove(removeList.get(i) - i);
            }
            removeList.clear();

            //subtraction
            for (int i = 0; i < Indexes.size(); i++) {
                if (s.charAt(Indexes.get(i)) == '-') {
                    doubles.set(i + 1, doubles.get(i) + doubles.get(i + 1));
                    removeList.add(i);
                }
            }
            for (int i = 0; i < removeList.size(); i++) {
                doubles.remove(removeList.get(i) - i);
                Indexes.remove(removeList.get(i) - i);
            }
            removeList.clear();

            return doubles.get(0);
        }
    }
    public static String rounding(String s)
    {
        String ss = s.substring(s.indexOf(".") + 1);
        if (ss.length() == 1 && ss.charAt(0) == '0') {
            return s.substring(0, s.indexOf("."));
        }
        else if (ss.length() > 4)
        {
            int i = Integer.parseInt(Character.toString(ss.charAt(3)));
            if (Integer.parseInt(Character.toString(ss.charAt(4))) > 4)
            {
                i++;
                ss = ss.substring(0, 3) + i;
                return s.substring(0, s.indexOf(".") + 1) + ss;
            }
            else
                return s.substring(0, s.indexOf(".") + 1) + ss.substring(0, 4);
        }
        else
            return s;
    }
}
