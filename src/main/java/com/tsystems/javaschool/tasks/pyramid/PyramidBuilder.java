package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            if (inputNumbers.size() > 200000000) throw new CannotBuildPyramidException(); //when more, doesn't enought heap memory
            int m = 1;
            int n = 1;
            for (; n < inputNumbers.size(); m++, n += m) {}
            if (n == inputNumbers.size()){
                int[] a = new int[inputNumbers.size()];
                for (int i = 0; i < inputNumbers.size(); i++) {
                    a[i] = inputNumbers.get(i);
                }
                quicksort(a, 0, a.length - 1);
                n = m;
                m = m * 2 - 1;
                int B[][] = new int[n][m];
                int k = 0;
                for (int i = 0; i < n; i++) {
                    B[i][n - i - 1] = a[k];
                    k++;
                    for (int j = 1; j < i + 1; j++) {
                        B[i][n - i - 1 + 2 * j] = a[k];
                        k++;
                    }
                }
                return B;
            }
            else throw new CannotBuildPyramidException();
        }
        catch (Exception e){
                throw new CannotBuildPyramidException();
            }
    }
    private static void quicksort(int[] A, int left, int right)
    {
        int i = left, j = right;
        int x = A[(left + right)/2], y;
        do {
            while ((A[i] < x) && (i < right)) i++;
            while ((x < A[j]) && (j > left)) j--;

            if (i <= j) {
                y = A[i];
                A[i] = A[j];
                A[j] = y;
                i++; j--;
            }
        } while (i <= j);

        if (left < j) quicksort(A, left, j);
        if (i < right) quicksort(A, i, right);
    }
}