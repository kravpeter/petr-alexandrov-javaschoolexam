# README #

This is a repo with T-Systems Java School preliminary examination tasks.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Petr Alexandrov
* Codeship : [ ![Codeship Status for kravpeter/petr-alexandrov-javaschoolexam](https://app.codeship.com/projects/fe2c9d80-feab-0135-8b1c-22030f554301/status?branch=master)](https://app.codeship.com/projects/279521)